/**
 * 
 */
package com.infix.git;

/**
 * @author mdahmed
 *
 */
public class DifferentVar {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int myIntValue= 5/3;
		System.out.println(myIntValue);// result will not give any decimal number .
		float myFloatValue = 5f/3f; //will give short decimal.
		System.out.println(myFloatValue);
		double myDoubleValue = 5d/3d;// will give longer decimal values as t has 64 bytes.
		System.out.println(myDoubleValue);

	}

}
